﻿using System;
using System.Net;
using System.Text;

namespace FreshDesk.Services
{
    public class Request
    {
        private const string fdDomain = "testcompanyiw1"; //freshdesk domain
        private const string apiKey = "BEoQ6aEqqQHhLNLLGGEq";

        public static HttpWebRequest SetRequest(string apiPath)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://" + fdDomain + ".freshdesk.com" + apiPath);
                request.ContentType = "application/json";
                request.Method = "GET";
                string authInfo = apiKey + ":X"; // It could be your username:password also.
                authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
                request.Headers["Authorization"] = "Basic " + authInfo;
                return request;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
