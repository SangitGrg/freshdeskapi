﻿using FreshDesk.Service.Tickets.Ticket;

namespace FreshDesk.Services.Ticket
{
    interface ITicketService
    {
        string Create(TicketModel obj);
        void Update();
        void Delete(string id);
        string GetById(int id);
        string ListAll();
    }
}
