﻿using FreshDesk.Service.Tickets.Ticket;
using System;

namespace FreshDesk.Services.Ticket
{
    public class TicketService : ITicketService
    {
        TicketRepository _tickets;

        public string Create(TicketModel obj)
        {
            return(_tickets.Create(obj));
        }

        public void Delete(string id)
        {
            throw new NotImplementedException();
        }

        public string GetById(int id)
        {
            return (_tickets.GetById(id));
        }

        public string ListAll()
        {
            return(_tickets.ListAll());
        }

        public void Update()
        {
            throw new NotImplementedException();
        }
    }
}
