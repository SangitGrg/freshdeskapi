﻿using FreshDesk.Service.Tickets.Ticket;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace FreshDesk.WebAPI.Controllers
{
    [Route("api/[controller]")]
    public class TicketController : Controller
    {
        TicketRepository _contacts = new TicketRepository();
     

        [HttpGet]
        public IEnumerable<TicketModel> Get()
        {
           
            var jsonObject = _contacts.ListAll();
            // var jsonObject = new JavaScriptSerializer().Serialize(test);
            var ticket = JsonConvert.DeserializeObject<List<TicketModel>>(jsonObject);
            return ticket;
        }
        [HttpGet("{id}")]
        public TicketModel Get(int id)
        {
            var jsonObject = _contacts.GetById(id);
            // var jsonObject = new JavaScriptSerializer().Serialize(test);
            var ticket = JsonConvert.DeserializeObject<TicketModel>(jsonObject);
            return ticket;
        }
        [HttpPost]
        public void Post(TicketModel obj)
        {
            _contacts.Create(obj);
        }
    }
}